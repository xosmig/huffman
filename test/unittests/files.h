
#ifndef _UNITTESTS_FILES_H_
#define _UNITTESTS_FILES_H_

#include "../../src/utility.hpp"
#include "../module/lib.h"

namespace unittests {
    namespace files {
        auto create_mod() -> module::Node;
    }
}

#endif // _UNITTESTS_FILES_H_
