
#include "tree.h"
#include "../../../src/huffman/tree.h"

#include <algorithm>
#include <sstream>

namespace unittests {
    namespace huffman {
        #define TEST_STR \
"Our Hero, our Hero claims a warrior's heart \
I tell you, I tell you the Dragonborn comes \
With a voice wielding power of ancient Nord Art \
Believe, believe the Dragonborn comes \
It's an end to the evil, of all Skyrim's foes \
Beware, beware the Dragonborn comes \
For the darkness has passed and the legend yet grows \
You'll know, you'll know Dragonborn's come."

        auto Tree::cnt_and_binary() -> void {
            auto tree = []() {
                std::stringstream ss;
                ss << TEST_STR;
                auto cnt_box = Impl16::count(ss);
                return Tree16::from_cnt(*cnt_box);
            }();

            auto bin = tree.to_binary();
            std::stringstream ss;
            Impl16::print_binary(ss, bin);

            test_assert(Tree16::from_binary_stream(ss).to_binary() == bin);
        }

        auto Tree::create_mod() -> module::Node {
            auto res = module::Node("tree");

            #define add_test(testname) res.add_child(make_box(module::Leaf(#testname, testname)));
            add_test(cnt_and_binary);

            return move(res);
        }
    }
}
