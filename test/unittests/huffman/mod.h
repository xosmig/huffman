
#ifndef _UNITTESTS_HUFFMAN_MOD_H_
#define _UNITTESTS_HUFFMAN_MOD_H_

#include "../../../src/utility.hpp"
#include "../../module/lib.h"

namespace unittests {
    namespace huffman {
        auto create_mod() -> module::Node;
    }
}

#endif // _UNITTESTS_HUFFMAN_MOD_H_
