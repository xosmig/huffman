
#include "mod.h"
#include "tree.h"

namespace unittests {
    namespace huffman {
        auto create_mod() -> module::Node {
            auto res = module::Node("huffman");

            res.add_child(make_box(Tree::create_mod()));

            return move(res);
        }
    }
}

