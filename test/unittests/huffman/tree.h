
#ifndef _TAIL_H_
#define _TAIL_H_

#include "../../../src/utility.hpp"
#include "../../module/lib.h"
#include "../../../src/huffman/tree.h"

namespace unittests {
    namespace huffman {
        class Tree {
            typedef ::huffman::Impl<uint16_t> Impl16;
            typedef Impl16::Tree Tree16;

            static auto cnt_and_binary() -> void;
        public:
            static auto create_mod() -> module::Node;
        };
    }
}

#endif // _TAIL_H_
