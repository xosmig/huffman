
#include "mod.h"
#include "files.h"
#include "parse_arguments.h"
#include "huffman/mod.h"

namespace unittests {
    auto create_mod() -> module::Node {
        auto res = module::Node("unittests");

        res.add_child(make_box(files::create_mod()));
        res.add_child(make_box(parse_arguments::create_mod()));
        res.add_child(make_box(huffman::create_mod()));

        return move(res);
    }
}
