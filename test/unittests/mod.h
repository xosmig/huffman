
#ifndef _UNITTESTS_MOD_H_
#define _UNITTESTS_MOD_H_

#include "../../src/utility.hpp"
#include "../module/lib.h"

namespace unittests {
    auto create_mod() -> module::Node;
}

#endif // _UNITTESTS_MOD_H_
