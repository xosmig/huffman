
#include "parse_arguments.h"
#include "../../src/parse_arguments.h"

namespace unittests {
    namespace parse_arguments {
        static
        auto encode() {
            {
                ConstStr args[] = { "name", "-c",  "--file", "in", "--output", "out" };
                auto res = ::parse_args(sizeof(args) / sizeof(ConstStr), args);
                test_assert(std::get<0>(res) == Operation::ZIP);
                test_assert(String(std::get<1>(res)) == "in");
                test_assert(String(std::get<2>(res)) == "out");
            }
            {
                ConstStr args[] = { "name", "-co",  "out", "-f", "in" };
                auto res = ::parse_args(sizeof(args) / sizeof(ConstStr), args);
                test_assert(std::get<0>(res) == Operation::ZIP);
                test_assert(String(std::get<1>(res)) == "in");
                test_assert(String(std::get<2>(res)) == "out");
            }
        }

        static
        auto decode() {
            {
                ConstStr args[] = { "name", "-u",  "--file", "in", "--output", "out" };
                auto res = ::parse_args(sizeof(args) / sizeof(ConstStr), args);
                test_assert(std::get<0>(res) == Operation::UNZIP);
                test_assert(String(std::get<1>(res)) == "in");
                test_assert(String(std::get<2>(res)) == "out");
            }
            {
                ConstStr args[] = { "name", "-uo",  "out", "-f", "in" };
                auto res = ::parse_args(sizeof(args) / sizeof(ConstStr), args);
                test_assert(std::get<0>(res) == Operation::UNZIP);
                test_assert(String(std::get<1>(res)) == "in");
                test_assert(String(std::get<2>(res)) == "out");
            }
        }

        static
        auto missing_type() {
            try {
                ConstStr args[] = { "name", "--file", "in", "--output", "out" };
                ::parse_args(sizeof(args) / sizeof(ConstStr), args);
            }
            catch(...) {
                return;
            }
            test_assert(false);
            
        }

        static
        auto missing_argument() {
            []() {
                try {
                    ConstStr args[] = { "name", "-c", "--file", "--output", "out" };
                    ::parse_args(sizeof(args) / sizeof(ConstStr), args);
                }
                catch(...) {
                    return;
                }
                test_assert(false);
            }();
            []() {
                try {
                    ConstStr args[] = { "name", "-cfo", "in", "out" };
                    ::parse_args(sizeof(args) / sizeof(ConstStr), args);
                }
                catch(...) {
                    return;
                }
                test_assert(false);
            }();
        }

        static
        auto invalid_flag() {
            []() {
                try {
                    ConstStr args[] = { "name", "-cf", "--file", "--output", "out", "--__unknown__" };
                    ::parse_args(sizeof(args) / sizeof(ConstStr), args);
                }
                catch(...) {
                    return;
                }
                test_assert(false);
            }();
            []() {
                try {
                    ConstStr args[] = { "name", "-cfx", "in", "-o", "out" };
                    ::parse_args(sizeof(args) / sizeof(ConstStr), args);
                }
                catch(...) {
                    return;
                }
                test_assert(false);
            }();
        }

        auto create_mod() -> module::Node {
            auto res = module::Node("parse_arguments");

            #define add_test(testname) res.add_child(make_box(module::Leaf(#testname, testname)));
            add_test(encode);
            add_test(decode);
            add_test(missing_type);
            add_test(missing_argument);
            add_test(invalid_flag);

            return move(res);
        }        
    }
}

