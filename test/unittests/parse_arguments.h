
#ifndef _UNITTESTS_PARSE_ARGUMENTS_H_
#define _UNITTESTS_PARSE_ARGUMENTS_H_

#include "../../src/utility.hpp"
#include "../module/lib.h"

namespace unittests {
    namespace parse_arguments {
        auto create_mod() -> module::Node;
    }
}

#endif // _UNITTESTS_PARSE_ARGUMENTS_H_
