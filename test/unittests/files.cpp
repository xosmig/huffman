
#include "files.h"
#include "../../src/files.h"

#include <cstring>

namespace unittests {
    namespace files {
        static auto TMPFILE = "___tmp~";
        static auto TMPFILE2 = "___tmp2~";

        static
        auto length() {
            // creates an empty file
            OFStream(TMPFILE, std::ios_base::binary);
            test_assert(::files::length(TMPFILE) == 0);

            OFStream(TMPFILE, std::ios_base::binary) << "hello";
            test_assert(::files::length(TMPFILE) == std::strlen("hello"));

            std::remove(TMPFILE);
        }

        static
        auto repeat() {
            OFStream(TMPFILE, std::ios_base::binary) << "hello";
            ::files::repeat(TMPFILE, TMPFILE2);

            {
                IFStream in(TMPFILE2, std::ios_base::binary);
                char ch;
                String res = "";
                while (in.get(ch)) {
                    res += ch;
                }
                test_assert(res == "hello");
            }

            std::remove(TMPFILE);
            std::remove(TMPFILE2);
        }

        auto create_mod() -> module::Node {
            auto res = module::Node("files");

            #define add_test(testname) res.add_child(make_box(module::Leaf(#testname, testname)));
            add_test(length);
            add_test(repeat);

            return move(res);
        }
    }
}
