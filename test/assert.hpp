

#ifndef _ASSERT_HPP_
#define _ASSERT_HPP_

#define test_assert(cond) if (!(cond)) { throw std::logic_error(#cond); }

#endif // _ASSERT_HPP_
