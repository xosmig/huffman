
#include "mod.h"
#include "../../src/do_work.h"
#include "../../src/files.h"

#include <algorithm>

namespace integrationtests {
    static auto TMPFILE = "___tmp~";
    static auto TMPFILE2 = "___tmp2~";

    auto create_mod() -> module::Node {
        auto res = module::Node("integrationtests");

        IFStream list("tests/list.test");

        String s;
        while (std::getline(list, s)) {
            if (!s.empty()) {
                res.add_child(make_box(new module::Leaf(s, 
                    [filename = "tests/" + s]() {
                        uint64_t original_size, encoded_size, additional_size;
                        {
                            ConstStr args[] = { "name", "-cf", filename.c_str(), "-o", TMPFILE };
                            auto res = ::do_work(sizeof(args) / sizeof(ConstStr), args);
                            original_size = res.before;
                            encoded_size = res.after;
                            additional_size = res.additional;

                            test_assert(::files::length(TMPFILE) == encoded_size + additional_size);
                        }

                        {
                            ConstStr args[] = { "name", "-uf", TMPFILE, "-o", TMPFILE2 };
                            auto res = ::do_work(sizeof(args) / sizeof(ConstStr), args);

                            test_assert(encoded_size == res.before);
                            test_assert(original_size == res.after);
                            test_assert(additional_size == res.additional);
                        }

                        {
                            auto in1 = IFStream(filename,  std::ios_base::binary);
                            auto in2 = IFStream(TMPFILE2,  std::ios_base::binary);

                            char ch1, ch2;
                            while (in1.get(ch1)) {
                                test_assert(in2.get(ch2));
                                test_assert(ch1 == ch2);
                            }
                            test_assert(!in2.get(ch2));
                        }

                        std::remove(TMPFILE);
                        std::remove(TMPFILE2);
                    }
                )));
            }
        }

        return move(res);
    }
}
