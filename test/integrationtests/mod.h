
#ifndef _INTEGRATIONTESTS_MOD_H_
#define _INTEGRATIONTESTS_MOD_H_

#include "../../src/utility.hpp"
#include "../module/lib.h"

namespace integrationtests {
    auto create_mod() -> module::Node;
}

#endif // _INTEGRATIONTESTS_MOD_H_
