/// using example: ./bin/test/main "integrationtests" "unittests :: files" "unittests :: parse_arguments :: encode"

#include "unittests/mod.h"
#include "integrationtests/mod.h"

int main(int argc, ConstStr argv[]) {
    auto root = module::Node("test");

    root.add_child(make_box(unittests::create_mod()));
    root.add_child(make_box(integrationtests::create_mod()));

    if (argc == 1) {
        root.run_name("unittests");
        root.run_name("integrationtests");
    } else {
        for (int i = 1; i < argc; ++i) {
            root.run_name(argv[i]);
        }
    }
}
