
#ifndef _MODULE_NODE_H_
#define _MODULE_NODE_H_

#include "base.h"

#include <map>

namespace module {
	class Node: public Base {
		typedef std::map<String, Box<Base>> Collection;

		Collection childs;

		auto run_iter(Collection::const_iterator it,
					  const String &prefix = "",
					  OStream &out = std::cout
					 ) const -> size_t;
	public:
		using Base::Base;

		auto add_child(Box<Base> child) -> void;

		auto run_name(const String &name, const String &prefix = "", OStream &out = std::cout) const -> size_t;
		auto run_all(const String &prefix = "", OStream &out = std::cout) const -> size_t;
	};
}

#endif // _MODULE_NODE_H_
