
#ifndef _MODULE_LEAF_H_
#define _MODULE_LEAF_H_

#include "base.h"
#include "../assert.hpp"

#include <functional>

namespace module {
    // For some reasons I've decided to use the `std::function` instead of template <class FuncT>
    class Leaf: public Base {
    public:
        typedef std::function<auto() -> void> Func;
    private:
        Func func;
    public:
        Leaf(const String &name, Func func): Base(name), func(func) {}
        auto run_name(const String &_name, const String &prefix = "", OStream &out = std::cout) const -> size_t;
        auto run_all(const String &prefix = "", OStream &out = std::cout) const -> size_t;
    };
}

#endif // _MODULE_LEAF_H_
