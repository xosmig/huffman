
#ifndef _MODULE_BASE_HPP_
#define _MODULE_BASE_HPP_

#include "../../src/utility.hpp"

namespace module {
    class Base {
    public:
        static constexpr ConstStr SEPARATOR = " :: ";
        static const size_t SEPARATOR_LEN;

        String name;
        Base(const String &name): name(name) {}

        virtual
        auto run_name(const String &name, const String &prefix = "", OStream &out = std::cout) const -> size_t = 0;

        virtual
        auto run_all(const String &prefix = "", OStream &out = std::cout) const -> size_t = 0;

        virtual
        ~Base() {};
    };
}

#endif // _MODULE_BASE_HPP_
