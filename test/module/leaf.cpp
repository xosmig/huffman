
#include "leaf.h"

namespace module {
    auto Leaf::run_name(const String &_name, const String &prefix, OStream &out) const -> size_t {
        debug_assert(_name == "");
        return run_all(prefix, out);
    }

    auto Leaf::run_all(const String &prefix, OStream &out) const -> size_t {
        out << prefix << name << " ... ";
        out.flush();
        try {
            func();
        }
        catch (std::exception &e) {
            out << "FAIL:\n" << e.what() << std::endl;
            return 0;
        }
        catch (...) {
            out << "FAIL:\nunknown_error" << std::endl;
            return 0;
        }
        out << "OK" << std::endl;
        return 1;
    }
}
