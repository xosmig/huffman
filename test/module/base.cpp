
#include "base.h"

#include <cstring>

namespace module {
    const size_t Base::SEPARATOR_LEN = std::strlen(SEPARATOR);
}
