
#include "node.h"
#include "../assert.hpp"

#include <algorithm>

namespace module {
    auto Node::add_child(Box<Base> child) -> void {
        debug_assert(childs.find(child->name) == childs.end());
        childs[child->name] = move(child);
    }

    auto Node::run_iter
            (Collection::const_iterator it, const String &prefix, OStream &out) const -> size_t {
        debug_assert(it != childs.end());
        return it->second->run_all(prefix + name + SEPARATOR, out);
    }

    auto Node::run_name(const String &path, const String &prefix, OStream &out) const -> size_t {
        if (path.empty()) {
            return run_all(prefix);
        }

        auto sep = path.find(SEPARATOR);
        auto next_path = (sep == String::npos ? String("") : path.substr(sep + SEPARATOR_LEN));

        auto it = childs.find(path.substr(0, sep));
        debug_assert(it != childs.end());

        return it->second->run_name(next_path, prefix + name + SEPARATOR, out);
    }

    auto Node::run_all(const String &prefix, OStream &out) const -> size_t {
        size_t res;
        for (auto it = childs.begin(); it != childs.end(); ++it) {
            res += run_iter(it,  prefix, out);
        }
        return res;
    }
}
