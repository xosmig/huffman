
#ifndef _HUFFMAN_IMPL_H_
#define _HUFFMAN_IMPL_H_

#include "../utility.hpp"

#include <limits>
#include <array>

namespace unittests{ namespace huffman{ class Tree; } }

namespace huffman {
    template <class Word>
    struct HuffmanTraits;

    template <>
    struct HuffmanTraits<uint8_t> {
        typedef uint_least16_t DoubleWord;
    };

    template <>
    struct HuffmanTraits<uint16_t> {
        typedef uint_least32_t DoubleWord;
    };

    template <class Word>
    class Impl {
        typedef typename HuffmanTraits<Word>::DoubleWord DoubleWord;
        static constexpr size_t WORD_SIZE = sizeof(Word) * BYTE_SIZE;
        static constexpr size_t ALP_SIZE = std::numeric_limits<Word>::max() + 1;

        struct Tail;
        typedef std::array<uint64_t, ALP_SIZE> CntCollection;

        class Tree;

        static auto read_word(IStream &in, Word &w) -> bool;
        static auto print_word(OStream &out, Word word) -> void;

        static auto count(IStream &in) -> Box<CntCollection>;
        static auto print_binary(OStream &out, const Vec<bool> &v) -> void;
        static auto print(IStream &in, OStream &out, const Tree &tree) -> void;

        friend class unittests::huffman::Tree;
    public:
        struct EncodeResult {
            uint64_t tree_size, encoded_data_size;
            uint8_t padding;
        };
        static auto encode(IStream &in, OStream &out) -> EncodeResult;

        struct DecodeResult {
            uint64_t tree_size, encoded_data_size;
            Word last_word;
        };
        static auto decode(IStream &in, OStream &out, uint8_t padding) -> DecodeResult;
    };
}

#endif // _HUFFMAN_IMPL_H_
