/// using for organise bit streams

#ifndef _HUFFMAN_TAIL_H_
#define _HUFFMAN_TAIL_H_

#include "impl.h"

namespace huffman {
    template <class Word>
    struct Impl<Word>::Tail {
        DoubleWord data = 0;
        DoubleWord len = 0;

        static auto empty() -> Tail {
            return Tail { 0, 0 };
        }

        auto operator+=(const Tail &other) -> void {
            data = ((data << other.len) | other.data);
            len += other.len;
            debug_assert(static_cast<size_t>(len) <= sizeof(data) * BYTE_SIZE);
        }

        auto print(OStream &out) const -> void {
            debug_assert(len < BYTE_SIZE);
            if (len > 0) {
                auto tail = *this;
                tail += Tail { 0, static_cast<DoubleWord>(BYTE_SIZE - tail.len) };
                out << static_cast<char>(tail.get(8));
            }
        }

        auto get(size_t count) const -> DoubleWord {
            debug_assert((data & ((1 << len) - 1)) == data);
            return data >> (len - count);
        }

        auto cut_val(size_t count) -> DoubleWord {
            auto res = get(count);
            len -= count;
            data &= (1 << len) - 1;
            return res;
        }

        auto get_byte(IStream &in) -> bool {
            char ch;
            if (in.get(ch)) {
                *this += Tail { static_cast<Byte>(ch), BYTE_SIZE };
                return true;
            }
            return false;
        }

        auto at_least_word(IStream &in) -> bool {
            while (len < WORD_SIZE) {
                if (!get_byte(in)) {
                    return false;
                }
            }
            return true;
        }
    };
}

#endif // _HUFFMAN_TAIL_H_
