
#include "node.h"

namespace huffman {
    template <class Word>
    Impl<Word>::Tree::Node::Node(Node &&other) noexcept:
        Node(move(other.zero), move(other.one), other.word, other.leaf)
    {
        debug_assert(other.parent == nullptr);

        if (zero) {
            zero->parent = this;
            zero->last_edge = 0;
        }

        if (one) {
            one->parent = this;
            one->last_edge = 1;
        }
    }

    // implicit instantiation
    template class Impl<uint8_t>::Tree::Node;
    template class Impl<uint16_t>::Tree::Node;
}
