
#ifndef _HUFFMAN_LIB_H_
#define _HUFFMAN_LIB_H_

#include "../utility.hpp"

#include <string>
#include <limits>

namespace huffman {
    struct Result {
        uint64_t before, after, additional;
    };

    /// doesn't compress anything, but makes a file which can be passed to `decode`
    auto encode0(const String &input, const String &output) -> Result;

    /// less efficient, with small header
    auto encode8(const String &input, const String &output) -> Result;

    /// more efficient, with big header
    auto encode16(const String &input, const String &output) -> Result;

    // for all types
    auto decode(const String &input, const String &output) -> Result;
}

#endif // _HUFFMAN_LIB_H_
