
#include "tree.h"
#include "tail.h"

#include <functional>
#include <algorithm>
#include <queue>

#include <iostream>

namespace huffman {
    template <class Word>
    auto Impl<Word>::Tree::from_cnt(const CntCollection &cnt) -> Tree {
        typedef std::pair<uint64_t, Box<Node>> QueueNode;

        auto iters_box = make_box(new ItersCollection);
        auto &iters = *iters_box;

        Vec<QueueNode> v;
        v.reserve(cnt.size());

        for (size_t i = 0; i < cnt.size(); ++i) {
            if (cnt[i] > 0) {
                v.emplace_back(
                    cnt[i],
                    make_box(Node::new_leaf(i))
                );
                iters[i] = v.back().second.get();
            }
        }

        if (v.size() == 1) {
            v.emplace_back(
                0,
                make_box(Node::new_leaf('\0'))
            );
        }

        auto comp = [](const QueueNode &a, const QueueNode &b){ return a.first > b.first; };

        std::priority_queue<
            QueueNode,
            Vec<QueueNode>,
            decltype(comp)
        > queue(comp, move(v));

        // `std::priority_queue` can't work with `std::unique_ptr` (aka `Box`) properly.
        auto pop_val = [&queue]() {
            auto res = move(const_cast<QueueNode&>(queue.top()));
            queue.pop();
            return move(res);
        };

        while (queue.size() > 1) {
            auto a = pop_val();
            auto b = pop_val();
            queue.emplace(
                a.first + b.first,
                make_box(Node::new_internal(move(a.second), move(b.second)))
            );
        }

        debug_assert(!queue.empty());
        auto tmp = pop_val();
        return Tree(move(tmp.second), move(iters_box));
    }

    template <class Word>
    auto Impl<Word>::Tree::get_code(Word word, Vec<bool> &out) const -> void {
        out.clear();

        auto iter = (*iters)[word];
        while (iter != root.get()) {
            out.push_back(iter->last_edge);
            iter = iter->parent;
        }

        std::reverse(out.begin(), out.end());
    }

    template <class Word>
    auto Impl<Word>::Tree::to_binary() const -> Vec<bool> {
        Vec<bool> res;

        // lambda functions can't be recursive without any overhead.
        class DfsT {
            Vec<bool> &v;
        public:
            DfsT(Vec<bool> &v): v(v) {}
            auto operator()(const Node &node) -> void {
                if (node.leaf) {
                    v.push_back(1);
                    for (size_t i = WORD_SIZE; i > 0;) {
                        --i;
                        v.push_back(static_cast<bool>((node.word >> i) & 1));
                    }
                } else {
                    v.push_back(0);
                    operator()(*node.zero);
                    operator()(*node.one);
                }
            }
        } dfs(res);

        dfs(*root);

        return move(res);
    }

    template <class Word>
    auto Impl<Word>::Tree::from_binary_stream(IStream &in) -> Tree {
        auto iters_box = make_box(new ItersCollection);

        // lambda functions can't be recursive without any overhead.
        class DfsT {
            IStream &in;
            Tail tail = Tail::empty();
            ItersCollection &iters;
        public:
            DfsT(IStream &in, ItersCollection &iters): in(in), iters(iters) {}
            auto operator()() -> Box<Node> {
                if (tail.len == 0) {
                    argument_assert(tail.get_byte(in), "TODO: Invalid archive file 1.");
                }

                bool leaf = tail.cut_val(1);

                if (leaf) {
                    argument_assert(tail.at_least_word(in), "TODO: Invalid archive file 2.");
                    return make_box(Node::new_leaf(tail.cut_val(WORD_SIZE)));
                } else {
                    auto zero = operator()();
                    auto one = operator()();
                    return make_box(Node::new_internal(move(zero), move(one)));
                }
            }
        } dfs(in, *iters_box);

        return Tree(dfs(), move(iters_box));
    }

    template <class Word>
    auto Impl<Word>::Tree::encode_stream(IStream &in, OStream &out) -> uint8_t {
        auto tail = Tail::empty();
        Word word;
        // a common variable to avoid reallocations.
        Vec<bool> code;

        while (read_word(in, word)) {
            get_code(word, code);
            for (bool bit: code) {
                tail += { bit, 1 };
                if (tail.len == BYTE_SIZE) {
                    out << static_cast<char>(tail.cut_val(BYTE_SIZE));
                }
            }
        }

        tail.print(out);
        return BYTE_SIZE - tail.len; // padding size
    }

    template <class Word>
    auto Impl<Word>::Tree::decode_stream(IStream &in, OStream &out, Byte padding) -> Word {
        Node *cur = root.get();

        std::pair<bool, Word> last = { false, 0 };
        while (true) {
            char ch;
            in.get(ch);
            auto tail = Tail { static_cast<Byte>(ch), BYTE_SIZE };

            bool last_byte = (in.peek() == std::ifstream::traits_type::eof());

            while (tail.len > 0) {
                bool bit = tail.cut_val(1);

                if (bit == 1) {
                    cur = (cur->one).get();
                } else {
                    cur = (cur->zero).get();
                }

                if (cur->leaf) {
                    if (last.first) {
                        print_word(out, last.second);
                    }
                    last = { true, cur->word };
                    cur = root.get();
                }

                if (last_byte && tail.len == padding) {
                    goto end;
                }
            }
        }

        end:;
        return last.second;
    }

    // implicit instantiation
    template class Impl<uint8_t>::Tree;
    template class Impl<uint16_t>::Tree;
}
