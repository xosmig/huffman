
#include "tail.h"

namespace huffman {
    // implicit instantiation
    template class Impl<uint8_t>::Tail;
    template class Impl<uint16_t>::Tail;
}
