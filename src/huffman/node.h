
#ifndef _HUFFMAN_NODE_H_
#define _HUFFMAN_NODE_H_

#include "tree.h"

namespace huffman {
    template <class Word>
    struct Impl<Word>::Tree::Node {
        Box<Node> zero = Box<Node>(nullptr);
        Box<Node> one = Box<Node>(nullptr);
        Node *parent = nullptr;
        Word word;
        bool leaf;
        bool last_edge;

        static auto new_leaf(Word word) -> Node {
            return  Node { Box<Node>(), Box<Node>(), word, true };
        }

        static auto new_internal(Box<Node> zero, Box<Node> one) -> Node {
            // the `parent` and `last_edge` fields will be set by the move constructor
            return Node { move(zero), move(one), 0, false };
        }

        Node(Node &&other) noexcept;

    private:
        Node(Box<Node> &&zero, Box<Node> &&one, Word word, bool leaf)
            noexcept:
            zero(move(zero)),
            one(move(one)),
            parent(nullptr),
            word(word),
            leaf(leaf),
            last_edge(0)
        {}
    };
}

#endif // _HUFFMAN_NODE_H_
