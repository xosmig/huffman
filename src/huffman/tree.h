
#ifndef _HUFFMAN_TREE_H_
#define _HUFFMAN_TREE_H_

#include "impl.h"

namespace huffman {
    template <class Word>
    class Impl<Word>::Tree {
    private:
        class Node;

        Box<Node> root;

        typedef std::array<Node*, ALP_SIZE> ItersCollection;
        Box<ItersCollection> iters;

        Tree(Box<Node> &&_root, Box<ItersCollection> &&_iters):
            root(move(_root)), iters(move(_iters)) {}

    public:
        typedef Impl<Word>::CntCollection CntCollection;

        Tree(Tree &&other) noexcept:
            Tree(move(other.root), move(other.iters)) {}

        static auto from_cnt(const CntCollection &cnt) -> Tree;
        static auto from_binary_stream(IStream &in) -> Tree;

        auto get_code(Word word, Vec<bool> &out) const -> void;
        auto to_binary() const -> Vec<bool>;

        auto encode_stream(IStream &in, OStream &out) -> uint8_t;
        auto decode_stream(IStream &in, OStream &out, Byte padding) -> Word;
    };
}

#include "node.h"

#endif // _HUFFMAN_TREE_H_
