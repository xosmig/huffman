#include "lib.h"
#include "impl.h"
#include "../files.h"

#include <fstream>
#include <exception>

namespace huffman {
    static
    auto open_files(const String &input, const String &output) {
        IFStream in(input, std::ios_base::binary);
        ios_assert(!in.fail(), "Unable to open input file " + input);

        OFStream out(output, std::ios_base::binary);
        ios_assert(!out.fail(), "Unable to open output file " + output);

        return make_pair(move(in), move(out));
    }

    static
    auto add_type(uint8_t padding, uint8_t type, const String &str) {
        OFStream(str, std::ios_base::in | std::ios_base::binary)
            .put(static_cast<char>((padding << 5) | last_bits(type, 5)));
    }

    auto encode0(const String &input, const String &output) -> Result {
        auto io = open_files(input, output);

        io.second.put(0);
        auto len = files::repeat(io.first, io.second);

        return Result { len, len, 1 };
    }

    auto encode8(const String &input, const String &output) -> Result {
        auto len = files::length(input);

        // expression-based language style (emulation).
        auto res = [&] {
            auto io = open_files(input, output);
            io.second.put(0);
            return Impl<uint8_t>::encode(io.first, io.second);
        }();
        add_type(res.padding, 0b01, output);

        return Result { len, res.encoded_data_size, res.tree_size + 1 };
    }

    auto encode16(const String &input, const String &output) -> Result {
        auto len = files::length(input);

        auto res = [&] {
            auto io = open_files(input, output);
            io.second << static_cast<char>(0);
            return Impl<uint16_t>::encode(io.first, io.second);
        }();
        add_type(res.padding, static_cast<uint8_t>(len % 2 == 1 ? 0b11 : 0b10), output);

        return Result { len, res.encoded_data_size, res.tree_size + 1 };
    }

    auto decode(const String &input, const String &output) -> Result {
        auto io = open_files(input, output);

        Byte first_byte = [&] {
            char ch;
            io.first.get(ch);
            return static_cast<Byte>(ch);
        }();
        auto padding = subword(first_byte, 0, 3);
        auto type = subword(first_byte, 3, 8);

        switch (type) {
            case 0b00: {
                auto len = files::repeat(io.first, io.second);
                return Result { len, len, 1 };
            }
            case 0b01: {
                auto res = Impl<uint8_t>::decode(io.first, io.second, padding);
                io.second.put(res.last_word);

                io.second.close();
                auto len = files::length(output);

                return Result { res.encoded_data_size, len, res.tree_size + 1 };
            }
            case 0b10: {
                auto res = Impl<uint16_t>::decode(io.first, io.second, padding);
                // print first byte
                io.second.put(subword(res.last_word, 0, BYTE_SIZE));
                // print second byte
                io.second.put(subword(res.last_word, BYTE_SIZE, 2 * BYTE_SIZE));

                io.second.close();
                auto len = files::length(output);

                return Result { res.encoded_data_size, len, res.tree_size + 1 };
            }
            case 0b11: {
                auto res = Impl<uint16_t>::decode(io.first, io.second, padding);
                // print only the first byte
                io.second << static_cast<char>(subword(res.last_word, 0, BYTE_SIZE));

                io.second.close();
                auto len = files::length(output);

                return Result { res.encoded_data_size, len, res.tree_size + 1 };
            }
            default: {
                argument_assert(false, "TODO: Invalid archive file 5");
                return { 0, 0, 0 }; // unreachable
            }
        }
    }
}
