
#include "impl.h"
#include "tree.h"
#include "tail.h"

#include <iostream>

namespace huffman {
    template <class Word>
    auto Impl<Word>::read_word(IStream &in, Word &w) -> bool {
        w = 0;
        size_t i;
        for (i = 0; i < sizeof(Word); ++i) {
            char ch;
            if (!in.get(ch)) {
                break;
            }
            w = (w << BYTE_SIZE) + static_cast<Byte>(ch);
        }

        return i > 0;
    }

    template <class Word>
    auto Impl<Word>::print_word(OStream &out, Word word) -> void {
        auto tail = Tail { word, WORD_SIZE };
        while (tail.len > 0) {
            out << static_cast<char>(tail.cut_val(BYTE_SIZE));
        }
    }

    template <class Word>
    auto Impl<Word>::count(IStream &in) -> Box<CntCollection> {
        auto cnt_box = make_box(new CntCollection);
        std::fill(cnt_box->begin(), cnt_box->end(), 0);

        Word word;

        while (read_word(in, word)) {
            ++(*cnt_box)[word];
        }

        return move(cnt_box);
    }

    template <class Word>
    auto Impl<Word>::print_binary(OStream &out, const Vec<bool> &v) -> void {
        Tail tail = Tail::empty();

        for (bool bit: v) {
            tail += Tail { bit, 1 };
            if (tail.len >= BYTE_SIZE) {
                out << static_cast<char>(tail.cut_val(BYTE_SIZE));
            }
        }

        tail.print(out);
    }

    template <class Word>
    auto Impl<Word>::encode(IStream &in, OStream &out) -> EncodeResult {
        auto tree = [&] {
            auto start = in.tellg();

            auto cnt_box = count(in);

            in.clear();
            in.seekg(start, std::ios_base::beg);

            return Tree::from_cnt(*cnt_box);
        }();

        uint64_t start = out.tellp();
        print_binary(out, tree.to_binary());
        uint64_t tree_size = static_cast<uint64_t>(out.tellp()) - start;

        start += tree_size;
        auto padding = tree.encode_stream(in, out);
        uint64_t encoded_data_size = static_cast<uint64_t>(out.tellp()) - start;

        return EncodeResult { tree_size, encoded_data_size, padding };
    }

    template <class Word>
    auto Impl<Word>::decode(IStream &in, OStream &out, uint8_t padding) -> DecodeResult {
        uint64_t start = in.tellg();
        auto tree = Tree::from_binary_stream(in);
        uint64_t tree_size = static_cast<uint64_t>(in.tellg()) - start;

        start += tree_size;
        auto last_word = tree.decode_stream(in, out, padding);

        // tellg() in the end of file is -1. So, I need this trick to fix it :(
        in.seekg(0, std::ios_base::end);
        uint64_t encoded_data_size = static_cast<uint64_t>(in.tellg()) - start;

        return DecodeResult { tree_size, encoded_data_size, last_word };
    }

    // implicit instantiation
    template class Impl<uint8_t>;
    template class Impl<uint16_t>;
}
