
#ifndef _UTILITY_HPP_
#define _UTILITY_HPP_

#include <string>
#include <cassert>
#include <memory>
#include <exception>
#include <vector>
#include <iostream>
#include <fstream>

// move:
using std::move;
using std::forward;

// Box:
template <class T>
using Box = std::unique_ptr<T>;

template <class T>
auto make_box(T &&obj) -> Box<T> {
    return Box<T>(new T(std::forward<T>(obj)));
}

template <class T>
auto make_box(T *ptr) -> Box<T> {
    return Box<T>(ptr);
}

// files:
typedef std::istream IStream;
typedef std::ifstream IFStream;

typedef std::ostream OStream;
typedef std::ofstream OFStream;

// asserts:
// `msg` will be calculated only in case of `!cond`.
#define release_assert(type, cond, msg) if (!(cond)) { throw type(msg); }

#define logic_assert(cond, msg) release_assert(std::logic_error, (cond), (msg))
#define argument_assert(cond, msg) release_assert(std::invalid_argument, (cond), (msg))
#define ios_assert(cond, msg) release_assert(std::ios_base::failure, (cond), (msg))

#define debug_assert assert

// other:
typedef unsigned char Byte;
constexpr size_t BYTE_SIZE = 8;

template<class T>
using Vec = std::vector<T>;

typedef std::string String;

typedef const char * ConstStr;

template <class T>
auto last_bits(T x, size_t cnt) -> T {
    return x & ((1 << cnt) - 1);
}

template <class T>
auto subword(T x, size_t first, size_t last) -> T {
    return last_bits(x >> (sizeof(T) * BYTE_SIZE - last), last - first);
}

#endif // _UTILITY_HPP_
