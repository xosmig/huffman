
#ifndef _DO_WORK_H_
#define _DO_WORK_H_

#include "huffman/lib.h"
#include "utility.hpp"

auto do_work(int argc, ConstStr *argv) -> huffman::Result;

#endif // _DO_WORK_H_
