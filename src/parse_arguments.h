
#ifndef _PARSE_ARGUMENTS_H_
#define _PARSE_ARGUMENTS_H_

#include "utility.hpp"

enum class Operation {
    NONE,
    ZIP,
    UNZIP
};

auto parse_args(int argc, ConstStr *argv) -> std::tuple<Operation, ConstStr, ConstStr>;

#endif // _PARSE_ARGUMENTS_H_
