
#include "do_work.h"

#include "files.h"
#include "parse_arguments.h"

auto do_work(int argc, ConstStr *argv) -> huffman::Result {
    auto parsed = parse_args(argc, argv);

    auto operation = std::get<0>(parsed);
    auto input = std::get<1>(parsed);
    auto output = std::get<2>(parsed);

    // if input file is empty
    if (IFStream(input).peek() == std::ifstream::traits_type::eof()) {
        files::repeat(input, output);
        return huffman::Result { 0, 0, 0 };
    }

    if (operation == Operation::ZIP) {
        auto res0 = huffman::encode0(input, "____res0.tmp~");
        auto len0 = res0.after + res0.additional;

        auto res8 = huffman::encode8(input, "____res8.tmp~");
        auto len8 = res8.after + res8.additional;

        auto res16 = huffman::encode16(input, "____res16.tmp~");
        auto len16 = res16.after + res16.additional;

        auto min = std::min(len0, std::min(len8, len16));

        if (min == len0) {
            std::rename("____res0.tmp~", output);
            std::remove("____res8.tmp~");
            std::remove("____res16.tmp~");
            return res0;
        } else if (min == len8) {
            std::rename("____res8.tmp~", output);
            std::remove("____res0.tmp~");
            std::remove("____res16.tmp~");
            return res8;
        } else {
            std::rename("____res16.tmp~", output);
            std::remove("____res0.tmp~");
            std::remove("____res8.tmp~");
            return res16;
        }
    } else {
        return huffman::decode(input, output);
    }
}
