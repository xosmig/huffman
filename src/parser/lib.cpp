
#include "lib.h"

#include <cstring>
#include <stdexcept>

auto Parser::operator()(int argc, ConstStr argv[]) const -> void {
    for (int i = 1; i < argc; ++i) {
        auto parse = [this, argc, &argv, i](std::string flag) -> int {
            auto it = handle.find(flag);
            argument_assert(it != handle.end(), "error: unknown flag " + flag);
            auto &h = it->second;

            auto param = (i + 1 < argc ? argv[i + 1] : nullptr);
            argument_assert(!h.has_param || param != nullptr,
                "error: missing parameter after " + flag);
            h.func(param);

            return h.has_param;
        };

        auto cur = argv[i];
        argument_assert(std::strlen(cur) >= 2 && cur[0] == '-', "error: missing flag");

        if (strlen(cur) >= 2 && cur[0] == '-') {
            if (cur[1] == '-') { // --flag
                i += parse(cur);
            } else { // -flags
                bool used = false;
                for (auto c = cur + 1; *c != '\0'; ++c) {
                    auto d = parse(std::string(1, *c));
                    if (d != 0) {
                        argument_assert(!used,
                            std::string("error: double usage of parameter ") + argv[i + 1]);
                        used = true;
                        ++i;
                    }
                }
            }
        }
    }
}
