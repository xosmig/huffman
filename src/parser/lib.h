
#ifndef _PARSER_LIB_H_
#define _PARSER_LIB_H_

#include "../utility.hpp"

#include <map>
#include <functional>
#include <string>

class Parser {
public:
    typedef std::function<auto (ConstStr) -> void> HandlerFunc;

    struct Handler {
        bool has_param;
        HandlerFunc func;
    };

    typedef std::map<std::string, Handler> Map;
private:
    Map handle;
public:
    Parser(Map && _handle): handle(_handle) {}
    auto operator()(int argc, ConstStr argv[]) const -> void;
};

#endif // _PARSER_LIB_H_
