
#ifndef _FILES_H_
#define _FILES_H_

#include "utility.hpp"

namespace files {
    auto length(IStream &in) -> uint64_t;
    auto length(const String &filename) -> uint64_t;
    auto repeat(IStream &in, OStream &out) -> uint64_t;
    auto repeat(const String &input, const String &output) -> uint64_t;
}

#endif // _FILES_H_
