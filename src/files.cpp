
#include "files.h"

namespace files {
    auto length(IStream &in) -> uint64_t {
        auto pos = in.tellg();
        in.seekg(0, std::ios_base::end);

        auto res = in.tellg();
        in.seekg(pos);

        return res;
    }

    auto length(const String &filename) -> uint64_t {
        IFStream in(filename);
        return length(in);
    }

    auto repeat(IStream &in, OStream &out) -> uint64_t {
        char ch;
        uint64_t size = 0;
        while (in.get(ch)) {
            out << ch;
            ++size;
        }
        return size;
    }

    auto repeat(const String &input, const String &output) -> uint64_t {
        IFStream in(input);
        OFStream out(output);
        return repeat(in, out);
    }

}
