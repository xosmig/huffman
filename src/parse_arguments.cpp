
#include "parse_arguments.h"

#include "parser/lib.h"

static const ConstStr HELP_MESSAGE = "// TODO: HELP_MESSAGE";

auto parse_args(int argc, ConstStr *argv) -> std::tuple<Operation, ConstStr, ConstStr> {
    auto operation = Operation::NONE;
    ConstStr input = nullptr;
    ConstStr output = nullptr;

    {
        Parser::Map handle;

        handle["c"] = { false, [&operation](ConstStr) { operation = Operation::ZIP; } };
        handle["u"] = { false, [&operation](ConstStr) { operation = Operation::UNZIP; } };

        Parser::Handler input_handler = { true, [&input](ConstStr param) { input = param; } };
        handle["f"] = input_handler;
        handle["--file"] = input_handler;

        Parser::Handler output_handler = { true, [&output](ConstStr param) { output = param; } };
        handle["o"] = output_handler;
        handle["--output"] = output_handler;

        Parser parser(move(handle));
        parser(argc, argv);
    }

    logic_assert(operation != Operation::NONE,
        std::string("error: missing operation\n") + HELP_MESSAGE);
    logic_assert(input != nullptr,
        std::string("error: missing input filename\n") + HELP_MESSAGE);
    logic_assert(output != nullptr,
        std::string("error: missing output filename\n") + HELP_MESSAGE);

    return std::make_tuple(operation, input, output);
}

