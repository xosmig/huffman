
#include "do_work.h"

auto main(int argc, ConstStr *argv) -> int {
    auto res = do_work(argc, argv);
    std::cout << res.before << std::endl;
    std::cout << res.after << std::endl;
    std::cout << res.additional << std::endl;
}
