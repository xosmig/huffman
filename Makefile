# Automatically generated.
# Useful targets: release, debug, test, clean.
# Executable files: `bin/debug/main/`, `bin/release/main`, `bin/test/main`.

MAKEFLAGS = -j 4

STD = c++1z

BUILD_O_RELEASE = g++ -O3 -c -Wfatal-errors -D NDEBUG -std=$(STD) $< -o $@
BUILD_O_DEBUG = g++ -c -Wall -std=$(STD) -g -Wextra -Wpedantic -Wfatal-errors $< -o $@
BUILD_OUT = g++ $^ -o $@

.PHONY: default release debug test clean

default: release

# release:

release: bin/release/main

bin/release/main: bin/release/main.o bin/release/do_work.o bin/release/parser/lib.o bin/release/files.o bin/release/parse_arguments.o bin/release/huffman/tree.o bin/release/huffman/impl.o bin/release/huffman/tail.o bin/release/huffman/node.o bin/release/huffman/lib.o
	mkdir -p bin/release || true
	$(BUILD_OUT)

bin/release/main.o: src/main.cpp src/do_work.h src/utility.hpp
	mkdir -p "bin/release/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/do_work.o: src/do_work.cpp src/do_work.h src/huffman/lib.h src/files.h src/parse_arguments.h src/utility.hpp src/utility.hpp
	mkdir -p "bin/release/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/parser/lib.o: src/parser/lib.cpp src/parser/lib.h src/utility.hpp
	mkdir -p "bin/release/parser/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/files.o: src/files.cpp src/files.h src/utility.hpp
	mkdir -p "bin/release/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/parse_arguments.o: src/parse_arguments.cpp src/parse_arguments.h src/parser/lib.h src/utility.hpp src/utility.hpp
	mkdir -p "bin/release/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/huffman/tree.o: src/huffman/tree.cpp src/huffman/tree.h src/huffman/tail.h src/huffman/impl.h src/huffman/node.h src/utility.hpp
	mkdir -p "bin/release/huffman/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/huffman/impl.o: src/huffman/impl.cpp src/huffman/impl.h src/huffman/tree.h src/huffman/tail.h src/utility.hpp src/huffman/node.h
	mkdir -p "bin/release/huffman/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/huffman/tail.o: src/huffman/tail.cpp src/huffman/tail.h src/huffman/impl.h src/utility.hpp
	mkdir -p "bin/release/huffman/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/huffman/node.o: src/huffman/node.cpp src/huffman/node.h src/huffman/tree.h src/huffman/impl.h src/utility.hpp
	mkdir -p "bin/release/huffman/" 2> /dev/null || true
	$(BUILD_O_RELEASE)

bin/release/huffman/lib.o: src/huffman/lib.cpp src/huffman/lib.h src/huffman/impl.h src/files.h src/utility.hpp
	mkdir -p "bin/release/huffman/" 2> /dev/null || true
	$(BUILD_O_RELEASE)


# debug:

debug: bin/debug/main

bin/debug/main: bin/debug/main.o bin/debug/do_work.o bin/debug/parser/lib.o bin/debug/files.o bin/debug/parse_arguments.o bin/debug/huffman/tree.o bin/debug/huffman/impl.o bin/debug/huffman/tail.o bin/debug/huffman/node.o bin/debug/huffman/lib.o
	mkdir -p bin/debug || true
	$(BUILD_OUT)

bin/debug/main.o: src/main.cpp src/do_work.h src/utility.hpp
	mkdir -p "bin/debug/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/do_work.o: src/do_work.cpp src/do_work.h src/huffman/lib.h src/files.h src/parse_arguments.h src/utility.hpp src/utility.hpp
	mkdir -p "bin/debug/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/parser/lib.o: src/parser/lib.cpp src/parser/lib.h src/utility.hpp
	mkdir -p "bin/debug/parser/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/files.o: src/files.cpp src/files.h src/utility.hpp
	mkdir -p "bin/debug/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/parse_arguments.o: src/parse_arguments.cpp src/parse_arguments.h src/parser/lib.h src/utility.hpp src/utility.hpp
	mkdir -p "bin/debug/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/huffman/tree.o: src/huffman/tree.cpp src/huffman/tree.h src/huffman/tail.h src/huffman/impl.h src/huffman/node.h src/utility.hpp
	mkdir -p "bin/debug/huffman/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/huffman/impl.o: src/huffman/impl.cpp src/huffman/impl.h src/huffman/tree.h src/huffman/tail.h src/utility.hpp src/huffman/node.h
	mkdir -p "bin/debug/huffman/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/huffman/tail.o: src/huffman/tail.cpp src/huffman/tail.h src/huffman/impl.h src/utility.hpp
	mkdir -p "bin/debug/huffman/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/huffman/node.o: src/huffman/node.cpp src/huffman/node.h src/huffman/tree.h src/huffman/impl.h src/utility.hpp
	mkdir -p "bin/debug/huffman/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/debug/huffman/lib.o: src/huffman/lib.cpp src/huffman/lib.h src/huffman/impl.h src/files.h src/utility.hpp
	mkdir -p "bin/debug/huffman/" 2> /dev/null || true
	$(BUILD_O_DEBUG)


# test:

test: bin/test/main

bin/test/main: bin/test/unittests/files.o bin/release/files.o bin/test/module/lib.o bin/test/module/node.o bin/test/module/leaf.o bin/test/module/base.o bin/test/unittests/parse_arguments.o bin/release/parse_arguments.o bin/release/parser/lib.o bin/test/unittests/huffman/tree.o bin/release/huffman/tree.o bin/release/huffman/tail.o bin/release/huffman/impl.o bin/release/huffman/node.o bin/test/unittests/huffman/mod.o bin/test/unittests/mod.o bin/test/integrationtests/mod.o bin/release/do_work.o bin/release/huffman/lib.o bin/test/test.o
	mkdir -p bin/test || true
	$(BUILD_OUT)

bin/test/unittests/files.o:  test/unittests/files.cpp test/unittests/files.h src/files.h src/utility.hpp test/module/lib.h test/module/node.h test/module/leaf.h test/module/base.h test/assert.hpp src/utility.hpp
	mkdir -p "bin/test/unittests/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/unittests/parse_arguments.o:  test/unittests/parse_arguments.cpp test/unittests/parse_arguments.h src/parse_arguments.h src/utility.hpp test/module/lib.h test/module/node.h test/module/leaf.h test/module/base.h test/assert.hpp src/utility.hpp
	mkdir -p "bin/test/unittests/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/unittests/huffman/tree.o:  test/unittests/huffman/tree.cpp test/unittests/huffman/tree.h src/huffman/tree.h src/utility.hpp test/module/lib.h src/huffman/impl.h src/huffman/node.h test/module/node.h test/module/leaf.h src/utility.hpp test/module/base.h test/assert.hpp src/utility.hpp
	mkdir -p "bin/test/unittests/huffman/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/unittests/huffman/mod.o:  test/unittests/huffman/mod.cpp test/unittests/huffman/mod.h test/unittests/huffman/tree.h src/utility.hpp test/module/lib.h src/huffman/tree.h test/module/node.h test/module/leaf.h src/huffman/impl.h src/huffman/node.h test/module/base.h test/assert.hpp src/utility.hpp src/utility.hpp
	mkdir -p "bin/test/unittests/huffman/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/unittests/mod.o:  test/unittests/mod.cpp test/unittests/mod.h test/unittests/files.h test/unittests/parse_arguments.h test/unittests/huffman/mod.h src/utility.hpp test/module/lib.h src/utility.hpp test/module/lib.h test/module/node.h test/module/leaf.h test/module/node.h test/module/leaf.h test/module/base.h test/assert.hpp test/module/base.h test/assert.hpp src/utility.hpp src/utility.hpp
	mkdir -p "bin/test/unittests/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/integrationtests/mod.o:  test/integrationtests/mod.cpp test/integrationtests/mod.h src/do_work.h src/utility.hpp test/module/lib.h test/module/node.h test/module/leaf.h test/module/base.h test/assert.hpp src/utility.hpp
	mkdir -p "bin/test/integrationtests/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/module/leaf.o:  test/module/leaf.cpp test/module/leaf.h test/module/base.h test/assert.hpp src/utility.hpp
	mkdir -p "bin/test/module/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/module/base.o:  test/module/base.cpp test/module/base.h src/utility.hpp
	mkdir -p "bin/test/module/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/module/node.o:  test/module/node.cpp test/module/node.h test/assert.hpp test/module/base.h src/utility.hpp
	mkdir -p "bin/test/module/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/module/lib.o:  test/module/lib.cpp test/module/lib.h test/module/node.h test/module/leaf.h test/module/base.h test/assert.hpp src/utility.hpp
	mkdir -p "bin/test/module/" 2> /dev/null || true
	$(BUILD_O_DEBUG)

bin/test/test.o:  test/test.cpp test/unittests/mod.h test/integrationtests/mod.h src/utility.hpp test/module/lib.h src/utility.hpp test/module/lib.h test/module/node.h test/module/leaf.h test/module/node.h test/module/leaf.h test/module/base.h test/assert.hpp test/module/base.h test/assert.hpp src/utility.hpp src/utility.hpp
	mkdir -p "bin/test/" 2> /dev/null || true
	$(BUILD_O_DEBUG)


# other:

clean:
	rm -r bin 2> /dev/null || true
	find \( -name "*.out" -or -name "*.o" -or -name "*.tmp" -or -name "*.zip" \) -and -delete

