#!/bin/bash

list="list.test"

printf "" > "$list"

while read file
do
    if [ "$file" ]
    then
        echo "$(basename "$file")" >> "$list"
    fi
done <<< "$(find -type f -not -name $list -not -name add_all.sh)"
